/**
 * Mozu Category Related Services Manager,
 * All manger functionalities are exposed as node module.
 * All mozu api request for Category services are done here.
 * Follwing are the services provided by this manager
 * 1. getCategories
**/

/**
 * Mozu Node SDK API Conext and Resources required for calling Mozu Category services.
**/
var apiContext = require('mozu-node-sdk/clients/platform/application')();
var categoryResource = require('mozu-node-sdk/clients/commerce/catalog/admin/category')(apiContext);


/* Method to fetch the existing Categories from mozu */
var getCategories = function(callback,res, extra, filter){
    categoryResource.getCategories(filter).then(function(categories){
        callback(null,categories,res,extra);
    }).catch(function(error) {
    	callback(error, null,res,extra);
    });
}


/**
 * Expose `category service`.
 */
var CategoryServiceManager =  {
    'getCategories'   : getCategories
}
module.exports = CategoryServiceManager
