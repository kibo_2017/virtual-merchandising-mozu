/**
 * Mozu Product Rule Related Services Manager
 * All manger functionalities are exposed as node module.
 * All mozu api request for Product rule services are done here.
 * Follwing are the services provided by this manager
 * 1. createProductRule
 * 2. updateProductRule
 * 3. getProductRules
 * 4. deleteProductRule
 * 3. getProductRule
**/

/**
 * Mozu API Conext and Resources required for calling Mozu product rule services.
**/
var apiContext = require('mozu-node-sdk/clients/platform/application')();
var serachTunigRuleResource = require('mozu-node-sdk/clients/commerce/catalog/admin/search')(apiContext);


/* Create a new product rule in mozu */
var createProductRule = function(callback,res,extra, newRule){
    serachTunigRuleResource.addSearchTuningRule(newRule).then( function(mozurs){
        callback(null,mozurs,res,extra);
    }).catch(function(error){
        callback(error,null,res,extra);
    });
}

/* update existing product rule in mozu */
var updateProductRule = function(callback,res,extra, updatedRule){
    serachTunigRuleResource.updateSearchTuningRule(updatedRule).then( function(mozurs){
        callback(null,mozurs,res,extra);
    }).catch(function(error){
        callback(error,null,res,extra);
    });
}

/* Method to fetch the existing product rules from mozu */
var getProductRules = function(callback,res, extra,filter){
    serachTunigRuleResource.getSearchTuningRules(filter).then(function(rules){
        callback(null,rules,res,extra);
    }).catch(function(error) {
    	callback(error, null,res,extra);
    });
}

/* Methode to delete product rule in mozu */
var deleteProductRule = function(callback,res,extra,ruleId){
    serachTunigRuleResource.deleteSearchTuningRule(ruleId).then( function(mozurs){
        callback(null,mozurs,res,extra);
    }).catch(function(error){
        callback(error,null,res,extra);
    });
}

/* Methode to fetch a particular rule from mozu */
var getProductRule = function(callback,res,extra,ruleId){
    serachTunigRuleResource.getSearchTuningRule(ruleId).then( function(rule){
        callback(null,rule,res,extra);
    }).catch(function(error){
        callback(error,null,res,extra);
    });
}

/**
 * Expose `product rule service`.
 */
var ProductRuleServiceManager =  {
    'getProductRules'       : getProductRules,
    'deleteProductRule'     : deleteProductRule,
    'updateProductRule'     : updateProductRule,
    'createProductRule'     : createProductRule,
    'getProductRule'        : getProductRule
}
module.exports = ProductRuleServiceManager
