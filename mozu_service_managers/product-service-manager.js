/**
 * Mozu Product Related Services Manager
 * All manger functionalities are exposed as node module.
 * All mozu api request for Product services are done here.
 * Follwing are the services provided by this manager
 * 1. getProducts
**/

/**
 * Mozu API Conext and Resources required for calling Mozu product services.
**/
var apiContext = require('mozu-node-sdk/clients/platform/application')();
// var productResource = require('mozu-node-sdk/clients/commerce/catalog/admin/product')(apiContext);
var productResource = require('mozu-node-sdk/clients/commerce/catalog/storefront/product')(apiContext);
var productSearchResource = require('mozu-node-sdk/clients/commerce/catalog/storefront/productSearchResult')(apiContext);

/**
 * Method to retrieves a list of products according to any specified facets,
 * filter criteria
**/
var getProducts = function(callback,res,extra,facetsFiltersSortOptions){
    productResource.getProducts(facetsFiltersSortOptions).then(function(products){
        callback(null,products,res,extra);
    }).catch(function(error) {
        console.log(error);
        callback(error, null,res,extra);
    });
}

/**
 * Method to retrive list of products based on the serach query and filter
**/
var searchProducts = function(callback,res,extra,facetsFiltersSortOptions){
    productSearchResource.search(facetsFiltersSortOptions).then(function(products){
        callback(null,products,res,extra);
    }).catch(function(error) {
        console.log(error);
        callback(error, null,res,extra);
    });
}

/**
 * Expose `product service`.
 */
var ProductServiceManager =  {
    'getProducts'   : getProducts,
    'searchProducts': searchProducts
}
module.exports = ProductServiceManager
