var express = require('express');
var router = express.Router();
var app = express();

var ProductRuleServiceManager  = require('../mozu_service_managers/product-rule-service-manager');
var CategoryServiceManager  = require('../mozu_service_managers/category-service-manager');
var CONSTANTS =  require('../constants/constants');
app.locals.CONSTANTS =  CONSTANTS;

/**
 * Handler for home page request,
 * call ProductRuleServiceManager.getProductRules, with callback_homepage
 * to render all the rules in to the dom.
**/
router.get('/', function(req, res, next) {
    // TODO : retrive filter values from request object
    var filter = {'startIndex':0,'pageSize':CONSTANTS.DEFAULT_RULES_PAGE_SIZE};
    var extra = {};
    ProductRuleServiceManager.getProductRules(callback_homepage,res,extra,filter);
});
/**
 * Methode to render the product rules useing the request.
**/
function callback_homepage(error,data,res,extra){
    if(!error){
        rules = data;
    }else{
        rules = [];
    }
    res.render('index', {
        product_rules : rules
    });
}


/* Ruter for handling different AJAX post request from view */

/* Methode to delete existing rule, one rule at a time */
router.post(CONSTANTS.SERVICE_URLS.deleteProductRule,function(req,res,next){
    var extra = req.body;
    var ruleCode = {
        "searchTuningRuleCode":extra.searchTuningRuleCode
    };
    ProductRuleServiceManager.deleteProductRule(callback,res,extra,ruleCode);
});

/**
 * Methode to handle delete rule api call,
 * send back the request body using extra param if success,
 * else send an error status of 500.
**/
function callback(error,data,res,extra){
    if(!error){
        res.send(extra);
    }else{
        console.log(error);
        res.sendStatus(500);
    }
}

/* Methode to new product rules : CONSTANTS.SERVICE_URLS.getProductRules*/
router.post(CONSTANTS.SERVICE_URLS.getProductRules,function(req,res,next){
    var filter = req.body;
    var extra = {
        template_name : CONSTANTS.TEMPLATES.rule_list
    };
    ProductRuleServiceManager.getProductRules(htmlResponseCallBack,res,extra,filter);
});

/**
 * Generic htmlResponseCallBack function.
 * htmlResponseCallBack function which will handle all the responses from the service managers
 * response data will be send back to view to show feedback in the view
 * If there is a template_name in exist in the extra parameter,
 * data will be rendered in the particular JADE template,
 * and an HTML will be send back using response object.
 * If no template file, json object will be send back.
**/
function htmlResponseCallBack(error,data,res,extra){
    if(!error){
        if(extra.template_name){
            var pathToTemplate = getpathToTemplate(extra.template_name);
            app.render(pathToTemplate, {
              data          : data,
              extra         : extra
            },function(err,html) {
              data = {
                  "html" : html,
                  "error": err
              }
              res.send(data)
            });
        }else{
            res.send(data);
        }
    }else{
        res.sendStatus(500);
    }
}

/**
 * Method to create a tempalte path, based on the name.
 * All templates will be kept under views/templates
 * with file extension of .jade
**/
function getpathToTemplate(name){
    return require('path').resolve(__dirname, '../views') + name + '.jade';
}


module.exports = router;
