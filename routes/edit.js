var express = require('express');
var router = express.Router();
var app = express();

var ProductRuleServiceManager  = require('../mozu_service_managers/product-rule-service-manager');
var CategoryServiceManager  = require('../mozu_service_managers/category-service-manager');
var ProductServiceManager  = require('../mozu_service_managers/product-service-manager');
var CONSTANTS =  require('../constants/constants');
app.locals.CONSTANTS =  CONSTANTS;
/**
 * Load basic page for editing, without choosing any particular category.
**/
router.get('/', function(req, res, next) {
    res.render('edit', {});
});
/**
 * Loading a particular category page,
 * All other data infomations will be fetched as part of the ajax calls.
**/
router.get(CONSTANTS.SERVICE_URLS.editCategory, function(req, res, next) {
    var categoryId = req.params.categoryId;
    res.render('edit', {
        categoryId                  : categoryId
    });
});

/* Ruter for handling different AJAX post request from view */

/**
 * Handlers for creating or editing an existing rule.
 * Retrive the ruleCode and rule object from request object
 * Fetch the product rules and
 * loop through the list to check if the rule is already existing
 * TODO : this api is returning all the rules instead of particular one.
 * TODO : update the service managers to return particular rule
 * If the rule is already exist call ProductRuleServiceManager.updateProductRule
 * If rule not exist call ProductRuleServiceManager.createProductRule
**/
router.post(CONSTANTS.SERVICE_URLS.createProductRule,function(req,res,next){
    var meThis = this;
    var newRule = req.body;
    var extra = {
        "searchTuningRuleCode"  : newRule.searchTuningRuleCode,
        "newRule"               : newRule
    };
    ProductRuleServiceManager.getProductRule(function(error,data,res,extra){
        var isExisting = false;
        for(var i=0; i < data.items.length; i++){
            if(extra.searchTuningRuleCode == data.items[i].searchTuningRuleCode){
                isExisting = true;
            }
        }
        if(isExisting){
            ProductRuleServiceManager.updateProductRule(htmlResponseCallBack,res, extra, extra.newRule);
        }else{
            ProductRuleServiceManager.createProductRule(htmlResponseCallBack, res, extra, extra.newRule);
        }
    },res, extra, extra.searchTuningRuleCode)
});

/* Handler for fetching all the categories and render them using a JADE template */
router.post(CONSTANTS.SERVICE_URLS.getCategories,function(req,res,next){
    var filter = req.body;
    var extra = {
        template_name : CONSTANTS.TEMPLATES.pages
    };
    CategoryServiceManager.getCategories(htmlResponseCallBack,res, extra,filter);
});

/* Handler for fetching products, and render using JADE template  */
router.post(CONSTANTS.SERVICE_URLS.getProducts,function(req,res,next){
    var filter = req.body;
    var extra = {
        template_name :  filter.template_name//CONSTANTS.TEMPLATES.product_list
    };
    ProductServiceManager.getProducts(htmlResponseCallBack,res, extra, filter);
});
/* Handler for searching products, and render using JADE template  */
router.post(CONSTANTS.SERVICE_URLS.searchProducts,function(req,res,next){
    var filter = req.body;
    var extra = {
        template_name :  filter.template_name//CONSTANTS.TEMPLATES.product_list
    };
    ProductServiceManager.searchProducts(htmlResponseCallBack,res, extra, filter);
});
/* Fetching the product rule */
router.post(CONSTANTS.SERVICE_URLS.getProductRules,function(req,res,next){

    var filter = req.body;
    var extra = {};
    ProductRuleServiceManager.getProductRule(htmlResponseCallBack,res, extra, filter);
});

/**
 * Generic htmlResponseCallBack function.
 * htmlResponseCallBack function which will handle all the responses from the service managers
 * response data will be send back to view to show feedback in the view
 * If there is a template_name in exist in the extra parameter,
 * data will be rendered in the particular JADE template,
 * and an HTML will be send back using response object.
 * If no template file, json object will be send back.
**/
function htmlResponseCallBack(error,data,res,extra){
    if(!error){
        if(extra.template_name){
            var pathToTemplate = getpathToTemplate(extra.template_name);
            app.render(pathToTemplate, {
              data          : data,
              extra         : extra
            },function(err,html) {
              data = {
                  "html" : html,
                  "error": err
              }
              res.send(data)
            });
        }else{
            res.send(data);
        }
    }else{
        res.send({
            'html':null,
            'error':error
        });
        // res.sendStatus(500);
    }
}

/**
 * Method to create a tempalte path, based on the name.
 * All templates will be kept under views/templates
 * with file extension of .jade
**/
function getpathToTemplate(name){
    return require('path').resolve(__dirname, '../views') + name + '.jade';
}

module.exports = router;
