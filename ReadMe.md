## Project Architecture Details
`🍺By Dipak Chandran P`

###### Application Flow:
 - Application will allow admin-user to create different product rules for each category.
 - Index page will list all available rules, from this page user can delete, edit or create a rule.
 - In edit page, user can select a particular category(page), all the products added into the rules will get listed here.
 - By dragging and dropping each product user can re-arrange the products in the rule.
 - User can also add new products to the rule by selecting new product from add product window.
 - User can find out the product by searching and sorting filters.
 - By clicking publish button a rule will be created and stored, if editing existing rule will be updated.
 - Each rule name, description and code will be dynamically created, based on the selected category id and name.

> Rule code creation logic : `SHINDIGZ-VM-STR-`+`categoryCode`

> Rule Name Creation logic : `categoryName.toUpperCase`+`(`+`categoryCode.toUpperCase`+`)`+`Ranking Rule`

> Rule Description : `Search tuning rule for category`+`categoryName`+` with id`+`categoryCode`


###### Application Modules Descriptions
- Constants
	* use to define the constants used in the app
- mozu_service_managers
	* Used to interact with different mozu services using mozu node sdk.
	* Each service manger with have appContext and serviceResouces.
	* Each service managers will expose serviceResouces based functionality.
	* service manager will accept 4 parameters:  callback,res, extra, data.
	* `callback` : is a function going to be invoked after interacting with mozu.
	* `res` : web service response object to deliver data to client.
	* `extra` : data which needed to be carried over from request to response.
	* `data` : request parameters required to interact with particular mozu service
- public
	* Contains all the files which is going to client machine.
	* Images, java-scripts and CSS files.
- rutes
	* Defines how application responds to different client requests.
	* `index.js`  : Home page, listing all the rules to its request
	* `edit.js` 	: Listen to all the rules related request
- view
	* JADE templates, used to responds with html markup to client requests.

###### Mozu app behaviour added:
- Product	: Product Read
- Product 	: Product Category Read
- Search 	: Create Product Ranking Definition
- Search 	: Read Product Ranking Definition
- Search 	: Update Product Ranking Definition
- Search	: Delete Product Ranking Definition

###### Third-party libraries used in front end
 - jQuery.js
    - Javascript library used for event handling and AJAX calls.
 - materialize.css
    - A modern responsive front-end framework based on Material Design
 - materialize.js
    - A modern responsive front-end framework based on Material Design
 - jquery.sortable.js
    - A jQuery plugin to create sortable lists and grids using native HTML5 drag and drop API.
