/* All common js functinalities will be defined here */
window.CONSTANTS = {};

$(function(){
    activateNavbar();
    // listener for modal popup
    $('.modal-trigger').leanModal();
});
/**
 * These functions will get invoked from the layout.jade file.
 * So each time the data comes, this will get updated.
**/
function preload_CONSTANTS(value){
	if(typeof value != 'undefined'){
		window.CONSTANTS = value;
	}else{
        window.CONSTANTS = null;
    }
    console.log(window.CONSTANTS);
}

/**
 * Methode to show rounded tost notification.
**/
function showToastNotification(message){
    Materialize.toast(message, 5000, 'rounded');
}

/**
 * Method to start and stop loader
 * isShow is true will show loader
 * else hide it.
**/
function switchPageLoader(isShow){
    if(isShow){
        $('.preloader-container').fadeIn();
    }else{
        $('.preloader-container').fadeOut();
    }
}

/* Based on the current url path this method will activate navbar */
function activateNavbar(){
    var path = window.location.pathname;
    if(path == "/"){
        $('.nav-wrapper a[href="/"]').parent().addClass('active');
        $('[data-publish="publish-rule"]').hide();
    }else{
        $('.nav-wrapper a[href="/edit"]').parent().addClass('active');
        $('[data-publish="publish-rule"]').show();
    }
}

window.categoryId = null;
/**
 * Method to assign the category id value to edit on.
 * This is invoked from the edit.jade file.
 * store the categoryId value as a global object to use
 * entaire application. If categoryId is undefined, the
 * global variable will be set as null.
**/
function preload_categoryId(category_Id){
    console.log("preloading category_Id : "+category_Id);
    if(typeof category_Id != 'undefined' ){
        window.categoryId = category_Id;
    }else{
        window.categoryId = null;
    }
}
