/**
 * Methode to create the rule object for current category
 *
**/
var ruleObject = {
    "active": true,
    "filters": [
        {
            "field": "categoryCode",
            "value": "selected category code"
        }
    ],
    "boostedProductCodes": " ordered list of product code string",
    "searchTuningRuleCode": "code generated based on the category id",
    "searchTuningRuleDescription": "Description generated based on the category name",
    "searchTuningRuleName": "Generated based on the category name",
    "siteId": 23651
}

function createCategoryRule(){
    var boostedProductCodes = getBoostedProductCodes();
    var filterFieldValue = getFilterFieldValue();
    var searchTuningRuleCode = getSearchTuningRuleCode();
    var searchTuningRuleDescription = getSearchTuningRuleDescription();
    var searchTuningRuleName = getSearchTuningRuleName();
    ruleObject.boostedProductCodes = boostedProductCodes;
    ruleObject.filters[0].value = filterFieldValue;
    ruleObject.searchTuningRuleCode = searchTuningRuleCode;
    ruleObject.searchTuningRuleDescription = searchTuningRuleDescription;
    ruleObject.searchTuningRuleName = searchTuningRuleName;

    addOrUpdateRule(ruleObject);
}

function addOrUpdateRule(ruleObject){
    console.log(ruleObject);
    switchPageLoader(true);
    $.ajax({
        type: "POST",
        url: window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.createProductRule,
        data: JSON.stringify(ruleObject),
        success: function(data){
            console.log(data);
            switchPageLoader(false);
            showToastNotification(data.searchTuningRuleName+" with id "+data.searchTuningRuleCode+"("+data.searchTuningRuleDescription+") has been created.");
        },
        dataType: 'json',
        contentType: "application/json; charset=utf-8"
    }).done(function(data) {
        console.log("finished rule api call");
    });
}

function getBoostedProductCodes(){
    var boostedProductCodes = [];
    $('[data-rule-products="rules-products"] li').each(function(){
        boostedProductCodes.push($(this).attr('data-productcode'));
    });
    return boostedProductCodes;
}

function getFilterFieldValue(){
    return $('li a[data-category-id].z-depth-1').attr('data-category-id');
}

function getSearchTuningRuleCode(){
    var categoryCode = $('li a[data-category-id].z-depth-1').attr('data-category-id');
    return 'SHINDIGZ-VM-STR-'+categoryCode;
}

function getSearchTuningRuleDescription(){
    var categoryCode = $('li a[data-category-id].z-depth-1').attr('data-category-id');
    var categoryName = $('li a[data-category-id].z-depth-1').text();
    return 'Search tuning rule for category '+categoryName+' with id '+categoryCode;
}

function getSearchTuningRuleName(){
    var categoryCode = $('li a[data-category-id].z-depth-1').attr('data-category-id');
    var categoryName = $('li a[data-category-id].z-depth-1').text();
    return categoryName.toUpperCase()+'('+categoryCode.toUpperCase()+') Ranking Rule';
}
