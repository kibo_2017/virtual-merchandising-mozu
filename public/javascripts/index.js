// JQUERY LOAD FUNCTION
$(function(){
    /**
     * Listening to delete button click event
    **/
    $('[data-rule-delete]').on('click',function(e){
        deleteRuleIfConfirm(e);
    });
    switchPageLoader(false);
    updatePageSize();
    /**
     * Listening to load more event for rules.
     * Get the current page size, increase the page size wrt to constant.
     * Call the function to get the new page sized rules
    **/
    $('[data-action="load-more-rules"]').on('click',function(e){
        e.preventDefault();
        if(!$('[data-action="load-more-rules"]').hasClass('disabled')){
            var currentPageSize = $('[data-action="load-more-rules"]').attr('data-pageSize');
            var startIndex = $('[data-action="load-more-rules"]').attr('data-startIndex');
            var newPageSize = parseInt(currentPageSize) + window.CONSTANTS.LOAD_MORE_ITEMS_SIZE;
            var filter = {
                'startIndex'  : startIndex,
                'pageSize'    : newPageSize
            };
            loadMoreRules(filter);
        }
    });
});
// JQUERY LOAD END

/**
 * Method to make api call to get the  ne list of rules.
 * Responsed HTML will returned will be appended to the current container.
**/
function loadMoreRules(filter){
    switchPageLoader(true);
    $.ajax({
        type: "POST",
        url: window.CONSTANTS.SERVICE_URLS.getProductRules,
        data: JSON.stringify(filter),
        success: function(data){
            $('[data-product-list]').children().remove();
            $('[data-product-list]').append(data.html);
            switchPageLoader(false);
            updatePageSize();
        },
        dataType: 'json',
        contentType: "application/json; charset=utf-8"
    }).done(function(data) {
        console.log("finished rule api call");
    });
}
/**
 * Method to update the page size
 * If currentPageSize is greater than or equals to product count, disable load more FAB
 * else update its data-pageSize, to current page size
**/
function updatePageSize(){
    var currentPageSize = $('[data-product-list]').children().length;
    var totalCount = parseInt($('[data-total-count]').attr('data-total-count'));
    if(currentPageSize == totalCount  || currentPageSize > totalCount){
        $('[data-action="load-more-rules"]').addClass('disabled');
    }else{
        $('[data-action="load-more-rules"]').attr('data-pageSize',currentPageSize);
    }
}

/**
 * Show prompt to end user to make confirmation of rule delete,
 * If confirmed, trigger api call for deleting product rule.
 * Once the respose came show tost notification and remove them corresponding
 * tabel row element from the table.
**/
function deleteRuleIfConfirm(e){
    var searchTuningRuleCode = $(e.currentTarget).attr('data-rule-delete');
    var searchTuningRuleName = $(e.currentTarget).attr('data-rule-name');
    var confirmed_delete = confirm("Are you sure you want to delete the rule "+searchTuningRuleName+" ( "+searchTuningRuleCode+" )?");
    var data = {
        "searchTuningRuleCode":searchTuningRuleCode,
        "searchTuningRuleName":searchTuningRuleName
    }
    if (confirmed_delete == true) {
        switchPageLoader(true);
        $.ajax({
            type: "POST",
            url: window.CONSTANTS.SERVICE_URLS.deleteProductRule,
            data: JSON.stringify(data),
            success: function(data){
                $('#rule_'+data.searchTuningRuleCode).remove()
                switchPageLoader(false);
                showToastNotification(data.searchTuningRuleName+'Rule has been deleted.');
            },
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        }).done(function(data) {
            console.log("finished rule api call");
        });
    }
}
