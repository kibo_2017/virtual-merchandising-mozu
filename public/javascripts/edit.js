/**
 * JS functionalities for handling events and interactions of edit page
 * Main functional fetures flow :
 * get rule based products, get category and filter based products.
 * Two type of product loading are there, 1. append, 2. new list
 * while filtering searchin new list is shown, while loading more
 * result is appended to the exsting products list.
**/

// JQUERY LOAD FUNCTION
$(function(){
    // To fetch and load the categories into dom, left side pages in the view.
    fetchCategories();
    addListeners();
});
// JQUERY LOAD END

// Method to add listener for different ui elemnts, and call corresponding function
function addListeners(){
    // Listening to the publish button click event.
    $('[data-publish="publish-rule"]').on('click',function(){
        publishRule();
    });
    // Listening to load more products button click event.
    $('[data-action="load-more-products"]').on('click',function(e){
        startLoadingMoreProducts(e);
    });
    // Listener for adding new products to the rule
    $('[data-action="add-products-to-rule"]').on('click',function(e){
        addNewProductsToRule(e);
    });

    // Listener for searching product
    $('[data-action="search-product"]').on('keyup',function(e){
        startSearchingProducts(e);
    });

    // Listener for sorting
    $('#dropdown-sorting li').on('click',function(e){
        sortProducts(e);
    });
}

/* Event listener functions START */
// Method will check if any category is selected, then
// will invoke rule creation function defined in rules.js
function publishRule(){
    if(window.categoryId!=null){
        createCategoryRule();
    }else{
        showToastNotification("Please select a page, to create a serach rule.");
    }
}
// Method to add new products into the rule
function addNewProductsToRule(e){
    var container = $('[data-rule-products="rules-products"]');
    $('[name="add-product-to-rule"]:checked').each(function(){
        // If the product is already existing do not add
        var id  = $(this).attr('id');
        if($('[data-rule-products="rules-products"]').find('li[data-productcode="'+id+'"]').length == 0){
            var newProducts  = $('[data-hidden-product-container="'+id+'"]').find('li.product.card');
            var newProduct = newProducts.clone();
            newProduct.css('display','inherit');
            newProduct.attr('data-productcode-remove',id);
            container.append(newProduct);
        }else{
            showToastNotification("Product with id : "+id+" already added to the rule.");
        }
    });
    activateDnD();
}
// Method to load more product
function startLoadingMoreProducts(e){
    if(!$(e.currentTarget).hasClass('disabled')){
        $('[data-action="search-product"]').val("");
        $('[data-value="current-sorting-value"]').html('Default');
        $('[data-value="current-sorting-value"]').attr('value','');
        createFilterAndLoadProducts(false,true,false);
    }
}
// Method to invoke searching products
function startSearchingProducts(e){
    $('[data-value="current-sorting-value"]').html('Default');
    $('[data-value="current-sorting-value"]').attr('value','');
    var search_key = $('[data-action="search-product"]').val();
    if(search_key.trim().length > CONSTANTS.MIN_SEARCH_CHARACTER || search_key.length == 0){
        createFilterAndLoadProducts(true,false,false);
    }
}
// Method to invoke sorting products
function sortProducts(e){
    var sortby = $(e.target).attr('value');
    var text = $(e.target).attr('text');
    $('[data-action="search-product"]').val("");
    $('[data-value="current-sorting-value"]').html(text);
    $('[data-value="current-sorting-value"]').attr('value',sortby);
    createFilterAndLoadProducts(false,false,true);
}
/* Event listener functions END */

// Method to create new filter object to fetch products
// Modify this method to include existing filtering option with the new result.
function createFilterAndLoadProducts(isSearching,isLoading,isSorting){
    var data = null;
    var isLoadRuleProducts = false;
    data = {
        startIndex  : 0,
        pageSize    : window.CONSTANTS.DEFAULT_PRODUCT_PAGE_SIZE,
        filter      : 'categoryId eq '+window.categoryId,
        template_name: CONSTANTS.TEMPLATES.product_list
    };
    if(isSearching){
        var search_key = $('[data-action="search-product"]').val();
        data.query = search_key;
    }else if (isLoading) {
        var pageSize = $('[data-products="products"]').children().length;
        data.pageSize = pageSize +window.CONSTANTS.LOAD_MORE_ITEMS_SIZE;
    }else if (isSorting) {
        var sortby = $('[data-value="current-sorting-value"]').attr('value');
        data.sortBy = sortby;
    }else{
        isLoadRuleProducts = true;
    }
    if(window.categoryId != null){
        fetchProducts(data,isLoadRuleProducts);
    }
}

/**
 * Method to fetch categories by AJAX call.
 * Retrieved HTML will be appended to dom,
 * and selected category will be highlighted.
**/
function fetchCategories(){
    var data = {};
    data.startIndex = 0;
    data.pageSize = window.CONSTANTS.DEFAULT_CATEGORY_PAGE_SIZE;

    $.ajax({
        type: "POST",
        url: window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.getCategories,
        data: JSON.stringify(data),
        success: function(data){
            var container = $('[data-categories="categories"]');
            if(typeof data.html == "undefined"){
                console.log(data);
            }else{
                container.append(data.html);
            }
            highlightSelectedCategory();
        },
        dataType: 'json',
        contentType: "application/json; charset=utf-8"
    }).done(function(data) {

    });
}

/* Methode to update the selected category in the page selection panel */
function highlightSelectedCategory(){
    $('[data-category-id]').removeClass('teal white-text z-depth-1');
    if(window.categoryId != null){
        $('[data-category-id="'+window.categoryId+'"]').addClass('teal white-text z-depth-1');
        createFilterAndLoadProducts(false,false,false);
    }else{
        switchPageLoader(false);
    }
}
/**
 * Method to fetch products for the selected category,
 * response html will be appended to DOM.
**/
function fetchProducts(data,isLoadRuleProducts){
    switchPageLoader(true);
    var url = window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.getProducts;
    if(data.query){
        url = window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.searchProducts;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        success: function(data){
            var container = $('[data-products="products"]');
            if(typeof data.html == "undefined"){
                console.log(data);
            }else{
                container.children().remove();
                container.append(data.html);
            }
            switchLoadMore();
            if(isLoadRuleProducts){
                loadRuleBasedProducts();
            }else{
                switchPageLoader(false);
            }
        },
        error: function(data){
            switchPageLoader(false);
        },
        dataType: 'json',
        contentType: "application/json; charset=utf-8"
    }).done(function(data) {

    });
}

// Method to disable or enable load more button in the product loading section.
function switchLoadMore(){
    var total_product = parseInt($("[data-total-products]").attr('data-total-products'));
    var pageSize = $('[data-products="products"]').children().length;
    if(total_product == pageSize || total_product < pageSize){
        $('[data-action="load-more-products"]').addClass("disabled");
    }else{
        $('[data-action="load-more-products"]').removeClass("disabled");
    }
}


/* Loading the products which are already exist in the rule. */
function loadRuleBasedProducts(){
    var searchTuningRuleCode = getSearchTuningRuleCode();
    var data = {"searchTuningRuleCode"  : searchTuningRuleCode};
    $.ajax({
        type: "POST",
        url: window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.getProductRules,
        data: JSON.stringify(data),
        success: function(data){
            renderRuleProductsIntoView(data);
        },
        error: function(data){
            switchPageLoader(false);
        },
        dataType: 'json',
        contentType: "application/json; charset=utf-8"
    }).done(function(data) {

    });
}


/**
 * Rendering products associated to the rules into view.
**/
function renderRuleProductsIntoView(data){
    var container = $('[data-rule-products="rules-products"]');
    container.children().remove();
    var filter = "";
    if(data.boostedProductCodes){
        for(var i=0;i<data.boostedProductCodes.length; i++){
            if(i!=0){
                filter = filter + " or productCode eq "+data.boostedProductCodes[i];
            }else{
                filter = "productCode eq "+data.boostedProductCodes[i];
            }
        }
        var data1 = {
            "pageSize": data.boostedProductCodes.length+4,
            "filter": filter,
            "template_name": CONSTANTS.TEMPLATES.rule_product_list
        };
        switchPageLoader(true);
        $.ajax({
            type: "POST",
            url: window.CONSTANTS.SERVICE_URLS.edit+window.CONSTANTS.SERVICE_URLS.getProducts,
            data: JSON.stringify(data1),
            success: function(data){
                container.append(data.html);
                activateDnD();
                switchPageLoader(false);
            },
            error: function(data){
                switchPageLoader(false);
            },
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        }).done(function(data) {

        });
    }else{
        switchPageLoader(false);
    }
}

/**
 * Activating Drag and Drop fetures to product list.
 * Method to manipulate the order of products listed.
**/
function activateDnD(){
    $('.sortable').sortable();
    activateProductRemoveHandler();
}

// Removing an product from the rule
function activateProductRemoveHandler(){
    $('[data-action="removeitem"]').on('click',function(e){
        var productcode = $(e.currentTarget).attr('productcode');
        $('li[data-productcode-remove="'+productcode+'"]').remove();
    });
}
