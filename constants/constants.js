/**
 * To keep the consistancy of application URLs and other constants
 * all the constant values are declared over here, and exported as CONSTANTS
 * node module.
 *
 * All the contstants are passed to the application context to accses from views.
 *
 * SITE: informations related to the site, can keep other meta informations
 * for site here and use them in view.
 * usage: #{CONSTANTS.SITE.TITLE}
 *
 * SERVICE_URLS : urls used for the applications, these variable will be passed
 * to front end also, to use in the post requests and to make links.
 * Update the SERVICE_URLS values to change the application URLs.
 * usage: #{CONSTANTS.SERVICE_URLS.getProducts}
 *
 * DEFAULT_PRODUCT_PAGE_SIZE : Is the number of products shown in each page default.
 * DEFAULT_RULES_PAGE_SIZE : Is the number of rules shown  in rules listing page by default.
 * DEFAULT_CATEGORY_PAGE_SIZE : Is the number of categories {pages), shown to user default,
 * keep this value large or as much as the number of categories available, to fetch all the
 * categories fetch at once.
 * usage: #{CONSTANTS.DEFAULT_RULES_PAGE_SIZE}
 *
 * TEMPLATES : Are the name of jade-template file names, used to response the AJAX callback,
 * with HTML structure.
 * usage: #{CONSTANTS.TEMPLATES.pages}
 *
 * LOAD_MORE_ITEMS_SIZE :  Number of items to be loaded more from ui, in product listing and
 * in rules listing these are used, more clean way for loding items that a pagination.
 * usage: #{CONSTANTS.LOAD_MORE_ITEMS_SIZE}
 *
 * LABELS : All the static labels value used in the application is defined
 * as part of this variable. usage: #{CONSTANTS.LABELS.save}
 *
 * MIN_SEARCH_CHARACTER :  number characters user has to input to start searching.
**/
var CONSTANTS = {
    'SITE'  :{
        "TITLE" : 'Shindigz'
    },
    'SERVICE_URLS'  : {
        "getProducts"       : '/get-products',
        "getCategories"     : '/get-categories',
        "getProductRules"   : '/get-rules',
        "createProductRule" : '/create-rule',
        "updateProductRule" : '/update-rule',
        "deleteProductRule" : '/delete-rule',
        "edit"              : '/edit',
        "editCategory"      : '/:categoryId',
        "searchProducts"      : '/serach',
    },
    'DEFAULT_PRODUCT_PAGE_SIZE'     : 10,
    'DEFAULT_CATEGORY_PAGE_SIZE'    : 100,
    'DEFAULT_RULES_PAGE_SIZE'       : 10,
    'TEMPLATES'     :{
        'pages'         : '/templates/pages',
        'product_list'  : '/templates/product-listing',
        'rule_list'     : '/templates/rules-listing',
        'rule_product_list'     : '/templates/rules-products'
    },
    'LOAD_MORE_ITEMS_SIZE'    : 10,
    'LABELS'    :{
        'edit_link'     :   'Add New Rule',
        'rules'         :   'Rules List',
        'save'          :   'Publish',
        'name_id'       :   'Rule Name and ID',
        'edit'          :   'Edit',
        'delete'        :   'Delete',
        'addnewproduct' :   'Add new product to rule',
        'addproduct'    :   'Add product to rule',
        'loadproduct'   :   'Load more products',
    },
    'SORTING_OPTIONS'   :[
        {value:"price+asc",text:" Price low-high"},
        {value:"price+desc",text:" Price high-low"},
        {value:"productName+asc",text:" Name a-z"},
        {value:"productName+desc",text:" Name z-a"},
        {value:"",text:" Default"}
    ],
    'MIN_SEARCH_CHARACTER': 5
}

module.exports = CONSTANTS
